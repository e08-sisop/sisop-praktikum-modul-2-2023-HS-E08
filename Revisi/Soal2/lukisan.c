#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<stdbool.h> 

/*FUNGSI DOWNLOAD*/
void download_file(char* dirName)
{
    int id_download;
    char picture_name[30], loc[50], linkURL[50];

    for(int i=0; i<15; i++){
        id_download = fork();
        if(id_download == 0){
            time_t t = time(NULL);
            struct tm* localTime = localtime(&t);
            strftime(picture_name, 30, "%Y-%m-%d_%H:%M:%S.png", localTime);
            sprintf(loc, "%s/%s", dirName, picture_name);
            sprintf(linkURL, "https://picsum.photos/%ld", (t % 1000) + 50);

            execlp("wget", "wget", "-q", "-O", loc, linkURL, NULL);
        }
        sleep(5);
    }
    while(wait(NULL) > 0);
}

/*FUNGSI ZIP*/
void make_zip(char* dirName)
{
    char filezip[30];
    sprintf(filezip, "%s.zip", dirName);

    int zid;
    zid = fork();
    if(zid == 0){
        execlp("zip", "zip", "-r", filezip, dirName, NULL);
    }
    while(wait(NULL) != zid);
}

/*FUNGSI REMOVE FOLDER*/
void rm_folder(char* dirName)
{
    execlp("rm", "rm", "-r", dirName, NULL);
}

/*FUNGSI UNTUK MEMBUAT KILLER*/
void create_kill(char *argv[], pid_t pid, pid_t sid)
{
    FILE *fileptr = fopen("killer.c", "w");

    // Killer.c
    char input[1024] = ""
    "#include <unistd.h>\n"
    "#include <wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
    "}\n";

    // Mode A
    char command[1024];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fileptr, input, command, "/usr/bin/pkill");
    }

    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fileptr, input, command, "/bin/kill");
    }

    fclose(fileptr);

    // Compile killer.c
    pid = fork();
    if(pid == 0)
    {
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", command);
    }
    while(wait(NULL) != pid);

    // Remove killer.c
    pid = fork();
    if (pid == 0) {
    	char *argv[] = {"rm", "killer.c", NULL};
    	execv("/bin/rm", argv);
    }
    while(wait(NULL) != pid);
}

int main(int argc, char *argv[]) {
  /*CEK ARGUMEN WAKTU EXEC LUKISAN*/
  if(argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b')) {
    printf("Argument tidak valid\nCara execute: ./lukisan -[a/b]\n");
    exit(0);
  }

  /*DAEMON*/
  pid_t pid, sid; 
  pid = fork();
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }
  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  /*PANGGIL FUNGSI CREATEKILLER KETIKA EXECUTE LUKISAN*/
  create_kill(argv, pid, sid);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) {
    int cid;
    time_t nowtime;
    struct tm *times;
    char dirName[50];  

    //buat folder
    time(&nowtime);
    times = localtime(&nowtime);
    strftime(dirName, sizeof(dirName), "%Y-%m-%d_%H:%M:%S", times);
    cid = fork();
    if(cid==0){
        execlp("mkdir", "mkdir", dirName, NULL);
    }
    int status1;
    wait(&status1);

    //donlod gambar, zipfolder, removefolder secara overlap
    cid = fork();
    if(cid==0){
        download_file(dirName);
        make_zip(dirName);
        rm_folder(dirName);
    }
    sleep(30);

  }
}
