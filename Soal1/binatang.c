#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <time.h>

#define BUFFER_SIZE 1024
#define DIREK_NAME "binatang"
void move_file(char* filename, char* source_dir, char* dest_dir);


// fungsi untuk menghapus direktori
int remove_directory(const char *path) {
    DIR *d = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;

    if (d) {
        struct dirent *p;

        r = 0;

        while (!r && (p = readdir(d))) {
            int r2 = -1;
            char *buf;
            size_t len;

            // skip current dir and parent dir
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
                continue;
            }

            len = path_len + strlen(p->d_name) + 2;
            buf = malloc(len);

            if (buf) {
                struct stat statbuf;

                snprintf(buf, len, "%s/%s", path, p->d_name);

                if (!stat(buf, &statbuf)) {
                    if (S_ISDIR(statbuf.st_mode)) {
                        r2 = remove_directory(buf);
                    } else {
                        r2 = unlink(buf);
                    }
                }

                free(buf);
            }

            r = r2;
        }

        closedir(d);
    }

    if (!r) {
        r = rmdir(path);
    }

    return r;
}


int main(void) {

// mendownload file
    CURL *curl;
    CURLcode res;
    FILE *fp;
    char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char outfilename[FILENAME_MAX] = "binatang.zip";

    // Initialize curl
    curl = curl_easy_init();
    if(curl) {
        // Set URL to download
        curl_easy_setopt(curl, CURLOPT_URL, url);

        // Set option to follow redirects
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        // Open file for writing
        fp = fopen(outfilename,"wb");
        if(fp == NULL) {
            printf("Error opening file for writing!\n");
            return 1;
        }

        // Set file as output
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        // Perform the request and capture the response
        res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            printf("Error downloading file: %s\n", curl_easy_strerror(res));
        }

        // Cleanup
        curl_easy_cleanup(curl);
        fclose(fp);
    }

//melakukan unzip
	pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Failed to create child process.\n");
        exit(1);
    } else if (pid == 0) { // child process
        char* args[] = {"unzip", "binatang.zip", "-d", "binatang", NULL};
        execvp(args[0], args);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File binatang.zip berhasil di-unzip ke direktori binatang.\n");
        } else {
            fprintf(stderr, "Gagal melakukan unzip pada file binatang.zip.\n");
        }
    }

//melakukan pemilihan secara acak

DIR *direk;
    struct dirent *ent;
    int count = 0;
    char *files[9];

    srand(time(NULL)); // inisialisasi seed untuk random

    direk = opendir(DIREK_NAME);
    if (direk != NULL) {
        while ((ent = readdir(direk)) != NULL) {
            // cek apakah file bukan direktori
            if (ent->d_type == DT_REG) {
                // alokasi memori untuk nama file dan simpan dalam array
                files[count] = malloc(strlen(ent->d_name) + 1);
                strcpy(files[count], ent->d_name);
                count++;
            }
        }
        closedir(direk);

        // pilih indeks file secara acak
        int index = rand() % count;

        // tampilkan pesan dengan nama file yang dipilih
        printf("Grape Kun melakukan penjagaan (%s)\n", files[index]);

        // free memory
        for (int i = 0; i < count; i++) {
            free(files[i]);
        }
    } else {
        perror("Failed to open directory");
        return EXIT_FAILURE;
    }


//membuat 3 direktori

	char* dir_names[] = {"HewanDarat", "HewanAmphibi", "HewanAir"}; // array yang berisi nama direktori yang akan dibuat

    int i;
    for (i = 0; i < 3; i++) // melakukan iterasi sebanyak tiga kali untuk membuat tiga direktori
    {
        int status = mkdir(dir_names[i], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // membuat direktori dengan nama yang ada pada array dan memberikan permission yang sama seperti sebelumnya

        if (status == 0) // handling jika direktori berhasil dibuat
        {
            printf("Directory '%s' created successfully\n", dir_names[i]);
        }
        else // handling jika direktori gagal dibuat
        {
            perror("Error creating directory");
        }
    }

//memfilter file sesuai direktori seperti di soal

	char* dir_name = "binatang"; // nama direktori yang berisi file gambar
    char* darat_dir_name = "HewanDarat"; // nama direktori untuk file gambar darat
    char* amphibi_dir_name = "HewanAmphibi"; // nama direktori untuk file gambar amphibi
    char* air_dir_name = "HewanAir"; // nama direktori untuk file gambar air

    DIR* dir = opendir(dir_name); // membuka direktori cobaunzip

    if (dir) // handling jika direktori berhasil dibuka
    {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) // melakukan iterasi seluruh file yang ada pada direktori
        {
            if (entry->d_type == DT_REG) // handling jika file yang ditemukan adalah regular file (bukan direktori)
            {
                char* filename = entry->d_name; // nama file

                if (strstr(filename, "darat")) // jika nama file mengandung kata 'darat'
                {
                    move_file(filename, dir_name, darat_dir_name); // pindahkan file ke direktori HewanDarat
                }
                else if (strstr(filename, "amphibi")) // jika nama file mengandung kata 'amphibi'
                {
                    move_file(filename, dir_name, amphibi_dir_name); // pindahkan file ke direktori HewanAmphibi
                }
                else if (strstr(filename, "air")) // jika nama file mengandung kata 'air'
                {
                    move_file(filename, dir_name, air_dir_name); // pindahkan file ke direktori HewanAir
                }
            }
        }

        closedir(dir); // menutup direktori cobaunzip
    }
    else // handling jika direktori gagal dibuka
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }


//melakukan zip folder

	pid_t did;

    // create child process
    did = fork();

    if (did == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
        exit(EXIT_SUCCESS);
    } else if (did > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }

//air

	pid_t aid;

    // create child process
    aid = fork();

    if (aid == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
        exit(EXIT_SUCCESS);
    } else if (aid > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }

//amphibi

	pid_t amid;

    // create child process
    amid = fork();

    if (amid == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
        exit(EXIT_SUCCESS);
    } else if (amid > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }


//Menghapus direktori setelah di zip

	// menghapus direktori binatang
    if (remove_directory("binatang") == 0) {
        printf("Direktori binatang berhasil dihapus\n");
    } else {
        printf("Direktori binatang gagal dihapus\n");
    }

    // menghapus direktori HewanDarat
    if (remove_directory("HewanDarat") == 0) {
        printf("Direktori HewanDarat berhasil dihapus\n");
    } else {
        printf("Direktori HewanDarat gagal dihapus\n");
    }

    // menghapus direktori HewanAir
    if (remove_directory("HewanAir") == 0) {
        printf("Direktori HewanAir berhasil dihapus\n");
    } else {
        printf("Direktori HewanAir gagal dihapus\n");
    }

    // menghapus direktori HewanAmphibi
    if (remove_directory("HewanAmphibi") == 0) {
        printf("Direktori HewanAmphibi berhasil dihapus\n");
    } else {
        printf("Direktori HewanAmphibi gagal dihapus\n");
    }


    return 0;
}

void move_file(char* filename, char* source_dir, char* dest_dir)
{
    char source_path[BUFFER_SIZE];
    char dest_path[BUFFER_SIZE];

    // membuat path lengkap untuk file sumber dan tujuan
    snprintf(source_path, BUFFER_SIZE, "%s/%s", source_dir, filename);
    snprintf(dest_path, BUFFER_SIZE, "%s/%s", dest_dir, filename);

    // memindahkan file dari sumber ke tujuan
    int status = rename(source_path, dest_path);

    if (status == 0) // handling jika file berhasil dipindahkan
    {
        printf("File '%s' moved to directory '%s'\n", filename, dest_dir);
    }
    else // handling jika file gagal dipindahkan
    {
        perror("Error moving file");
    }
}
