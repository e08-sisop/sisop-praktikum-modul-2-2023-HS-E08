#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>

#define URL "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define FLNAME "players.zip"
#define BUFFER_SIZE 1024

void move_file(char* filename, char* source_dir, char* dest_dir);



int main(void) {

// mendownload file
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char url[256] = {0};
    int a = 0;

    // initialize curl
    curl = curl_easy_init();
    if (!curl) {
        printf("Failed to initialize curl\n");
        return 1;
    }

    snprintf(url, sizeof(url), "%s", URL);
    curl_easy_setopt(curl, CURLOPT_URL, url);


    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);


    fp = fopen(FLNAME, "wb");
    if (!fp) {
        printf("Failed to open file %s\n", FLNAME);
        return 1;
    }

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);


    res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        printf("Failed to download file %s (curl error: %s)\n", FLNAME, curl_easy_strerror(res));
        return 1;
    }


    curl_easy_cleanup(curl);
    fclose(fp);

    printf("File downloaded successfully\n");

//melakukan unzip

    pid_t pid = fork();

    if (pid == 0) {
        // child process
        char *args[] = {"unzip", "players.zip", NULL};
        execvp("unzip", args);
        exit(0);
    } else if (pid > 0) {
        // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Unzip successful.\n");
        } else {
            printf("Unzip failed.\n");
        }
    } else {
        // fork failed
        printf("Fork failed.\n");
        exit(1);
    }

//menghapus file players.zip
  // membuka file players.zip
    int fd = open("players.zip", O_RDONLY);
    if (fd == -1) {
        perror("Error while opening the file");
        exit(EXIT_FAILURE);
    }

    // menghapus file players.zip
    if (unlink("players.zip") == -1) {
        perror("Error while deleting the file");
        exit(EXIT_FAILURE);
    }

    // menutup file
    close(fd);


//memfilter file hanya man.united
char* dir_path = "players";
    DIR* direc = opendir(dir_path);
    if (direc == NULL) {
        perror("Error opening directory");
        return 1;
    }

    struct dirent* entry;
    struct stat file_stat;

    while ((entry = readdir(direc)) != NULL) {
        char file_path[100];
        sprintf(file_path, "%s/%s", dir_path, entry->d_name);

        if (stat(file_path, &file_stat) == -1) {
            perror("Error getting file status");
            continue;
        }

        if (S_ISREG(file_stat.st_mode) && strstr(entry->d_name, "ManUtd") == NULL) {
            if (unlink(file_path) == -1) {
                perror("Error deleting file");
            } else {
                printf("File %s deleted\n", entry->d_name);
            }
        }
    }

    closedir(direc);

//membuat folder bek, gelandang, kiper, penyerang

char *dir_names[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
   int i, parent_dir_fd;
    for (i = 0; i < 4; i++) {
        if ((parent_dir_fd = open(".", O_RDONLY)) == -1) {
            perror("Error opening parent directory");
            return 1;
        }
        if (mkdirat(parent_dir_fd, dir_names[i], 0700) == -1) {
            perror("Error creating directory");
            return 1;
        }
        if (close(parent_dir_fd) == -1) {
            perror("Error closing parent directory");
            return 1;
        }
    }
    printf("Directories created successfully.\n");


//memfilter file sesuai direktori seperti di soal

	char* dir_name = "players"; // nama direktori yang berisi file player
    char* kiper_dir_name = "Kiper"; // nama direktori untuk file gambar kiper
    char* bek_dir_name = "Bek"; // nama direktori untuk file gambar bek
    char* gelandang_dir_name = "Gelandang"; // nama direktori untuk file gambar gelandang
	char* penyerang_dir_name = "Penyerang";

    DIR* dir = opendir(dir_name); // membuka direktori

    if (dir) // handling jika direktori berhasil dibuka
    {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) // melakukan iterasi seluruh file yang ada pada direktori
        {
            if (entry->d_type == DT_REG) // handling jika file yang ditemukan adalah regular file (bukan direktori)
            {
                char* filename = entry->d_name; // nama file

                if (strstr(filename, "Kiper")) // jika nama file mengandung kata 'kiper'
                {
                    move_file(filename, dir_name, kiper_dir_name); // pindahkan file ke direktori Kiper
                }
                else if (strstr(filename, "Bek")) // jika nama file mengandung kata 'bek'
                {
                    move_file(filename, dir_name, bek_dir_name); // pindahkan file ke direktori Bek
                }
                else if (strstr(filename, "Gelandang")) // jika nama file mengandung kata 'Gelandang'
                {
                    move_file(filename, dir_name, gelandang_dir_name); // pindahkan file ke direktori Gelandang
                }
		else if (strstr(filename, "Penyerang")) // jika nama file mengandung kata penyerang
                {
                    move_file(filename, dir_name, penyerang_dir_name); // pindahkan ke penyerang
                }

            }
        }

        closedir(dir); // menutup direktori
    }
    else // handling jika direktori gagal dibuka
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }


return 0;
}


void move_file(char* namafile, char* scdirektori, char* d_dir)
{
    char source_path[BUFFER_SIZE];
    char dest_path[BUFFER_SIZE];

    // membuat path lengkap untuk file sumber dan tujuan
    snprintf(source_path, BUFFER_SIZE, "%s/%s", scdirektori, namafile);
    snprintf(dest_path, BUFFER_SIZE, "%s/%s", scdirektori, namafile);

    // memindahkan file dari sumber ke tujuan
    int status = rename(source_path, dest_path);

    if (status == 0) // handling jika file berhasil dipindahkan
    {
        printf("File '%s' moved to directory '%s'\n", namafile, d_dir);
    }
    else // handling jika file gagal dipindahkan
    {
        perror("Error moving file");
    }
}
