# Sisop Praktikum Modul 2 2023 HS E08

Anggota Kelompok    : 
- Ilham Insan Wafi 5025211255
- Dilla Wahdana 5025211060
- Muhammad Ahyun Irsyada 5025211251

# No.1
**Pada soal ini, kita diminta untuk menyelesaikan beberapa hal, yaitu:**

1. Grape-kun harus mendownload file https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
2. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
3. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
4. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

**Penyelesaian dari masalah diatas yaitu:**
```#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <time.h>

#define BUFFER_SIZE 1024
#define DIREK_NAME "binatang"
void move_file(char* filename, char* source_dir, char* dest_dir);


// fungsi untuk menghapus direktori
int remove_directory(const char *path) {
    DIR *d = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;

    if (d) {
        struct dirent *p;

        r = 0;

        while (!r && (p = readdir(d))) {
            int r2 = -1;
            char *buf;
            size_t len;

            // skip current dir and parent dir
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
                continue;
            }

            len = path_len + strlen(p->d_name) + 2;
            buf = malloc(len);

            if (buf) {
                struct stat statbuf;

                snprintf(buf, len, "%s/%s", path, p->d_name);

                if (!stat(buf, &statbuf)) {
                    if (S_ISDIR(statbuf.st_mode)) {
                        r2 = remove_directory(buf);
                    } else {
                        r2 = unlink(buf);
                    }
                }

                free(buf);
            }

            r = r2;
        }

        closedir(d);
    }

    if (!r) {
        r = rmdir(path);
    }

    return r;
}


int main(void) {

// mendownload file
    CURL *curl;
    CURLcode res;
    FILE *fp;
    char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char outfilename[FILENAME_MAX] = "binatang.zip";

    // Initialize curl
    curl = curl_easy_init();
    if(curl) {
        // Set URL to download
        curl_easy_setopt(curl, CURLOPT_URL, url);

        // Set option to follow redirects
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        // Open file for writing
        fp = fopen(outfilename,"wb");
        if(fp == NULL) {
            printf("Error opening file for writing!\n");
            return 1;
        }

        // Set file as output
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        // Perform the request and capture the response
        res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            printf("Error downloading file: %s\n", curl_easy_strerror(res));
        }

        // Cleanup
        curl_easy_cleanup(curl);
        fclose(fp);
    }

//melakukan unzip
	pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Failed to create child process.\n");
        exit(1);
    } else if (pid == 0) { // child process
        char* args[] = {"unzip", "binatang.zip", "-d", "binatang", NULL};
        execvp(args[0], args);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File binatang.zip berhasil di-unzip ke direktori binatang.\n");
        } else {
            fprintf(stderr, "Gagal melakukan unzip pada file binatang.zip.\n");
        }
    }

//melakukan pemilihan secara acak

DIR *direk;
    struct dirent *ent;
    int count = 0;
    char *files[9];

    srand(time(NULL)); // inisialisasi seed untuk random

    direk = opendir(DIREK_NAME);
    if (direk != NULL) {
        while ((ent = readdir(direk)) != NULL) {
            // cek apakah file bukan direktori
            if (ent->d_type == DT_REG) {
                // alokasi memori untuk nama file dan simpan dalam array
                files[count] = malloc(strlen(ent->d_name) + 1);
                strcpy(files[count], ent->d_name);
                count++;
            }
        }
        closedir(direk);

        // pilih indeks file secara acak
        int index = rand() % count;

        // tampilkan pesan dengan nama file yang dipilih
        printf("Grape Kun melakukan penjagaan (%s)\n", files[index]);

        // free memory
        for (int i = 0; i < count; i++) {
            free(files[i]);
        }
    } else {
        perror("Failed to open directory");
        return EXIT_FAILURE;
    }


//membuat 3 direktori

	char* dir_names[] = {"HewanDarat", "HewanAmphibi", "HewanAir"}; // array yang berisi nama direktori yang akan dibuat

    int i;
    for (i = 0; i < 3; i++) // melakukan iterasi sebanyak tiga kali untuk membuat tiga direktori
    {
        int status = mkdir(dir_names[i], S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // membuat direktori dengan nama yang ada pada array dan memberikan permission yang sama seperti sebelumnya

        if (status == 0) // handling jika direktori berhasil dibuat
        {
            printf("Directory '%s' created successfully\n", dir_names[i]);
        }
        else // handling jika direktori gagal dibuat
        {
            perror("Error creating directory");
        }
    }

//memfilter file sesuai direktori seperti di soal

	char* dir_name = "binatang"; // nama direktori yang berisi file gambar
    char* darat_dir_name = "HewanDarat"; // nama direktori untuk file gambar darat
    char* amphibi_dir_name = "HewanAmphibi"; // nama direktori untuk file gambar amphibi
    char* air_dir_name = "HewanAir"; // nama direktori untuk file gambar air

    DIR* dir = opendir(dir_name); // membuka direktori cobaunzip

    if (dir) // handling jika direktori berhasil dibuka
    {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) // melakukan iterasi seluruh file yang ada pada direktori
        {
            if (entry->d_type == DT_REG) // handling jika file yang ditemukan adalah regular file (bukan direktori)
            {
                char* filename = entry->d_name; // nama file

                if (strstr(filename, "darat")) // jika nama file mengandung kata 'darat'
                {
                    move_file(filename, dir_name, darat_dir_name); // pindahkan file ke direktori HewanDarat
                }
                else if (strstr(filename, "amphibi")) // jika nama file mengandung kata 'amphibi'
                {
                    move_file(filename, dir_name, amphibi_dir_name); // pindahkan file ke direktori HewanAmphibi
                }
                else if (strstr(filename, "air")) // jika nama file mengandung kata 'air'
                {
                    move_file(filename, dir_name, air_dir_name); // pindahkan file ke direktori HewanAir
                }
            }
        }

        closedir(dir); // menutup direktori cobaunzip
    }
    else // handling jika direktori gagal dibuka
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }


//melakukan zip folder

	pid_t did;

    // create child process
    did = fork();

    if (did == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
        exit(EXIT_SUCCESS);
    } else if (did > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }

//air

	pid_t aid;

    // create child process
    aid = fork();

    if (aid == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
        exit(EXIT_SUCCESS);
    } else if (aid > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }

//amphibi

	pid_t amid;

    // create child process
    amid = fork();

    if (amid == 0) {
        // child process
        execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
        exit(EXIT_SUCCESS);
    } else if (amid > 0) {
        // parent process
        wait(NULL);
        printf("Zip completed.\n");
    } else {
        // fork failed
        fprintf(stderr, "Fork failed.\n");
        exit(EXIT_FAILURE);
    }


//Menghapus direktori setelah di zip

	// menghapus direktori binatang
    if (remove_directory("binatang") == 0) {
        printf("Direktori binatang berhasil dihapus\n");
    } else {
        printf("Direktori binatang gagal dihapus\n");
    }

    // menghapus direktori HewanDarat
    if (remove_directory("HewanDarat") == 0) {
        printf("Direktori HewanDarat berhasil dihapus\n");
    } else {
        printf("Direktori HewanDarat gagal dihapus\n");
    }

    // menghapus direktori HewanAir
    if (remove_directory("HewanAir") == 0) {
        printf("Direktori HewanAir berhasil dihapus\n");
    } else {
        printf("Direktori HewanAir gagal dihapus\n");
    }

    // menghapus direktori HewanAmphibi
    if (remove_directory("HewanAmphibi") == 0) {
        printf("Direktori HewanAmphibi berhasil dihapus\n");
    } else {
        printf("Direktori HewanAmphibi gagal dihapus\n");
    }


    return 0;
}

void move_file(char* filename, char* source_dir, char* dest_dir)
{
    char source_path[BUFFER_SIZE];
    char dest_path[BUFFER_SIZE];

    // membuat path lengkap untuk file sumber dan tujuan
    snprintf(source_path, BUFFER_SIZE, "%s/%s", source_dir, filename);
    snprintf(dest_path, BUFFER_SIZE, "%s/%s", dest_dir, filename);

    // memindahkan file dari sumber ke tujuan
    int status = rename(source_path, dest_path);

    if (status == 0) // handling jika file berhasil dipindahkan
    {
        printf("File '%s' moved to directory '%s'\n", filename, dest_dir);
    }
    else // handling jika file gagal dipindahkan
    {
        perror("Error moving file");
    }
}

```
**Penjelasan dari source code diatas yaitu:**
1. Mendownload file menggunakan libcurl.

    Pada bagian pertama program, dilakukan proses untuk mendownload sebuah file dari URL yang ditentukan menggunakan library `libcurl`. Pertama, library ini diinisialisasi dengan menggunakan `curl_easy_init()`. Kemudian, URL yang akan didownload ditentukan menggunakan `curl_easy_setopt()` dengan argumen `CURLOPT_URL`. Untuk mengatur agar library mengikuti redirect, digunakan `curl_easy_setopt()` dengan argumen `CURLOPT_FOLLOWLOCATION`. Selanjutnya, dilakukan pembukaan file untuk menyimpan hasil download menggunakan `fopen()`. Kemudian, output dari library diset agar ditulis ke dalam file menggunakan `curl_easy_setopt()` dengan argumen `CURLOPT_WRITEDATA`. Terakhir, dilakukan eksekusi library menggunakan `curl_easy_perform()` dan file yang sudah dibuka ditutup menggunakan `fclose()`.
2. Melakukan unzip pada file yang didownload

    Setelah file berhasil didownload, dilakukan proses untuk melakukan unzip pada file tersebut menggunakan command `unzip`. Proses ini dilakukan dengan menggunakan `fork()` untuk membuat child process. Di dalam child process, digunakan `execvp()` untuk menjalankan command unzip dan mengarahkan hasil output ke direktori `binatang`. Di dalam parent process, digunakan `waitpid()` untuk menunggu child process selesai dieksekusi. Jika hasil exit status dari child process adalah `0`, maka pesan berhasil akan ditampilkan. Jika tidak, pesan gagal akan ditampilkan. Setelah proses unzip berhasil dilakukan, program akan membaca semua file di dalam direktori binatang dan memasukkannya ke dalam array. Kemudian, dilakukan pengambilan acak satu file dari array tersebut dan menampilkan pesan dengan nama file yang dipilih.
3. Membuat tiga direktori dan memfilter file hewan sesuai tempat tinggal

    Setelah proses pemilihan file secara acak berhasil dilakukan, program akan membuat tiga direktori dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Proses pembuatan direktori dilakukan menggunakan command `mkdir` dan dilakukan iterasi sebanyak tiga kali untuk membuat ketiga direktori tersebut. Setelah direktori berhasil dibuat, pesan berhasil akan ditampilkan.

    Selanjutnya, program akan memindahkan file yang dipilih secara acak pada langkah 3 ke salah satu dari tiga direktori yang sudah dibuat pada langkah 4. Proses ini dilakukan dengan menggunakan fungsi `move_file()` yang menerima tiga argumen yaitu nama file yang akan dipindahkan, direktori sumber, dan direktori tujuan. Fungsi ini menggunakan `rename()` untuk memindahkan file dari direktori sumber ke direktori tujuan. Jika proses pemindahan berhasil dilakukan, pesan berhasil akan ditampilkan. Jika tidak, pesan gagal akan ditampilkan.

    Adapun penjelasan dari fungsi `move_file` yaitu digunakan untuk memindahkan sebuah file dari direktori sumber ke direktori tujuan. Penjelasan singkat fungsi tersebut yaitu Pada awal fungsi, terdapat tiga parameter yaitu `filename`, `source_dir`, dan `dest_dir` yang digunakan untuk menyimpan nama file yang akan dipindahkan, direktori sumber, dan direktori tujuan masing-masing. Selanjutnya, dibuat dua variabel lokal yaitu `source_path` dan `dest_path` dengan tipe char dan ukuran sebesar BUFFER_SIZE (yang diasumsikan telah didefinisikan sebelumnya), yang akan digunakan untuk menyimpan path lengkap dari file sumber dan tujuan masing-masing. Kemudian, menggunakan fungsi `snprintf`, path lengkap untuk file sumber dan tujuan masing-masing dibuat dengan menggabungkan `source_dir` dan `dest_dir` dengan filename. Setelah itu, menggunakan fungsi `rename`, file dari sumber dipindahkan ke tujuan yang ditentukan oleh `dest_path`. Jika file berhasil dipindahkan, maka akan dicetak pesan sukses beserta nama file dan direktori tujuan menggunakan fungsi `printf`. Namun, jika file gagal dipindahkan, maka akan dicetak pesan error menggunakan fungsi `perror`.
4. Zip folder

    Pada bagian ini, program menggunakan fungsi `fork()` untuk membuat `child process` dan `parent process`. Proses anak akan melakukan proses zip pada direktori "HewanDarat", "HewanAir", dan "HewanAmphibi" menggunakan perintah `"zip"` pada terminal. Sedangkan, proses induk akan menunggu proses anak selesai melakukan proses zip sebelum melanjutkan ke proses selanjutnya. Jika proses fork() gagal, program akan menampilkan pesan error.
5. Menghapus file direktori

    Untuk menghapus file direktori, pada program ini menggunakan fungsi `remove_directory()`. Penjelasan dari fungsi tersebut yaitu pada awal fungsi terdapat satu parameter yaitu `path` yang merupakan path dari direktori yang akan dihapus. Selanjutnya, dibuat sebuah pointer `DIR` yaitu `d` yang akan digunakan untuk membuka direktori yang akan dihapus menggunakan fungsi opendir. Kemudian, variabel `path_len` digunakan untuk menyimpan panjang dari path menggunakan fungsi strlen. Variabel `r` didefinisikan sebagai -1 sebagai tanda bahwa penghapusan belum berhasil dilakukan. Jika direktori berhasil dibuka menggunakan `opendir`, maka dilakukan perulangan menggunakan `readdir` untuk membaca seluruh isi dari direktori. Selama perulangan, dilakukan pengecekan apakah direktori saat ini adalah direktori yang valid dan bukan direktori '.' atau '..'. Jika iya, maka perulangan akan dilanjutkan ke file atau direktori selanjutnya. Jika file atau direktori valid, variabel `len` digunakan untuk menyimpan panjang dari path baru yang akan dibuat dengan menggabungkan path dengan nama file atau direktori saat ini. Selanjutnya, digunakan fungsi `malloc` untuk mengalokasikan memori sebesar `len` dan membuat `buf` sebagai pointer ke memori yang dialokasikan tersebut. Jika pengalokasian memori berhasil, maka dilakukan pengecekan terhadap tipe dari file atau direktori saat ini menggunakan fungsi `stat`. Jika tipe file atau direktori adalah direktori, maka fungsi `remove_directory` akan dipanggil secara rekursif untuk menghapus direktori tersebut beserta seluruh isinya. Namun, jika tipe file atau direktori adalah file, maka menggunakan fungsi `unlink` file tersebut akan dihapus. Setelah file atau direktori saat ini berhasil dihapus, dilakukan dealokasi memori yang telah dialokasikan sebelumnya menggunakan fungsi free. Jika penghapusan seluruh isi dari direktori berhasil dilakukan, variabel `r` akan diubah nilainya menjadi 0. Setelah selesai melakukan penghapusan seluruh isi direktori, fungsi `rmdir` digunakan untuk menghapus direktori utama menggunakan path yang telah disimpan pada awal fungsi. Terakhir, nilai `r` akan dikembalikan sebagai tanda apakah penghapusan berhasil dilakukan atau tidak.

**Output dari source code diatas:**


![](image/output1_1.png)



![](image/output1_2.png)

![](image/ss_file_1.png)

# No.2
**Pada soal ini, kita diminta untuk menyelesaikan beberapa hal, yaitu:**
1. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
2. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
3. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
4. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
5. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

**Penyelesaian dari masalah diatas yaitu:**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <dirent.h>
#include <signal.h>

#define FOLDER_NAME_SIZE 20
#define IMAGE_NAME_SIZE 50

volatile sig_atomic_t stop = false;

void sigint_handler(int signum) {
    stop = true;
}

void zip_folder(char *folder_name) {
    char zip_command[100];
    sprintf(zip_command, "zip -qr %s.zip %s", folder_name, folder_name);
    system(zip_command);
}

void delete_folder(char* folder_name) {
    struct dirent *entry;
    DIR *dir = opendir(folder_name);
    char path[PATH_MAX];

    if (dir == NULL) {
        perror("Error while opening the directory");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(path, PATH_MAX, "%s/%s", folder_name, entry->d_name);

            if (entry->d_type == DT_DIR) {
                delete_folder(path);
            } else {
                unlink(path);
            }
        }
    }

    closedir(dir);

    // delete the empty directory
    rmdir(folder_name);
}


int main() {
    int i, j;
    char folder_name[FOLDER_NAME_SIZE], image_name[IMAGE_NAME_SIZE];
    struct tm *tm;
    time_t t;

    // setup signal handler for SIGINT
    signal(SIGINT, sigint_handler);

    while (!stop) {
        // generate folder name
        time(&t);
        tm = localtime(&t);
        strftime(folder_name, FOLDER_NAME_SIZE, "%Y-%m-%d_%H:%M:%S", tm);

        // create folder
        mkdir(folder_name, 0777);

        // download images
        for (i = 0; i < 15; i++) {
            // generate image name
            time(&t);
            tm = localtime(&t);
            sprintf(image_name, "%s/%s_%02d.jpg", folder_name, folder_name, i+1);

            // generate image URL
            char url[30];
            int size = (int)t%1000 + 50;
            sprintf(url, "https://picsum.photos/%d", size);

            // download image
            pid_t pid = fork();
            if (pid == 0) {
                execlp("wget", "wget", "-q", "-O", image_name, url, NULL);
                exit(0);
            }

            // wait for 5 seconds before downloading next image
            sleep(5);
        }

        // wait for child processes to finish
        while (wait(NULL) != -1);

        // zip folder
        zip_folder(folder_name);

        // delete folder
        delete_folder(folder_name);

        // wait for 30 seconds before creating next folder
        sleep(30);
    }

    return 0;
}

```

**Penjelasan dari source code diatas yaitu:**
Program di atas merupakan program yang akan mengunduh 15 gambar setiap 30 detik dari situs web `picsum.photos`, kemudian menyimpan gambar-gambar tersebut dalam folder yang dibuat dengan format nama folder "tahun-bulan-tanggal_jam:menit:detik". Setelah selesai mengunduh gambar-gambar, program akan mengompresi folder menjadi file .zip dan menghapus folder yang telah dibuat tadi. Proses ini akan terus berulang sampai program dihentikan oleh pengguna dengan menekan tombol `Ctrl+C`.

Program menggunakan beberapa header file seperti `stdio.h`, `stdlib.h`, `string.h`, `unistd.h`, `sys/stat.h`, `sys/types.h`, `time.h`, `fcntl.h`, `errno.h`, `libgen.h`, `sys/wait.h`, `dirent.h`, dan `signal.h`.

Program menggunakan beberapa fungsi-fungsi seperti `mkdir()`, `system()`, `opendir()`, `readdir()`, `snprintf()`, `strcmp()`, `strftime()`, `execlp()`, `exit()`, `sleep()`, `wait()`, dan `unlink()`.

Fungsi `sigint_handler()` digunakan untuk menangani sinyal `SIGINT (tombol Ctrl+C)` sehingga program dapat berhenti dengan aman.

Fungsi `zip_folder()` digunakan untuk mengompresi folder yang telah dibuat menggunakan perintah "zip" dari sistem operasi.

Fungsi `delete_folder()` digunakan untuk menghapus folder beserta isinya.

Program menggunakan variabel global stop yang bertipe data `volatile sig_atomic_t` untuk menandakan apakah program harus berhenti atau tidak.

Program menggunakan perulangan while untuk terus membuat folder, mengunduh gambar-gambar, mengompresi folder, dan menghapus folder selama variabel stop bernilai false.

Pada bagian pengunduhan gambar, program menggunakan `fork()` untuk membuat proses anak yang akan menjalankan perintah `wget` dari sistem operasi. Anak proses ini kemudian akan dihentikan menggunakan perintah `exit()` setelah selesai menjalankan perintah `wget`. Proses induk menunggu anak proses selesai menggunakan perintah `wait()`.

Setelah semua gambar telah diunduh, program akan mengompresi folder menjadi file .zip menggunakan fungsi `zip_folder()`, dan menghapus folder menggunakan fungsi `delete_folder()`.

Program akan tidur selama 30 detik sebelum membuat folder baru dan memulai kembali proses pengunduhan gambar. Pada setiap pengunduhan gambar, program akan tidur selama 5 detik sebelum mengunduh gambar berikutnya.

**Hasil programnya:**

![](image/ss_2.png)

**Kendala :**
- Pada soal nomor 2 ini kelompok kami masih kesulitan dalam membuat program killer di poin soal C dan D, sehingga kami hanya bisa menyelesaikan soal A dan B.
- Penyelesaian soal A dan B pun masih sedikit keliru dalam hal waktunya.

**Revisi :**
- Terlampir pada direktori revisi.


# No.3
**pada soal no 3 kita di suruh untuk melakukan beberapa hal sebagai berikut :** 
1. Mengunduh file yang berisikan database yang berisikan      database pemainbola,Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
2. Menghapus semua pemain yang bukan dari Manchester United yang ada di directory
3. Mengategorikan Anggota pemain sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda untuk kategori folder akan menjadi 4 yaitu Kiper,Bek,Gelandang,dan Penyerang.
4. Mencari kesebelasan terbaik berdasarkan ratting terbagus dengan adanya kiper,bek,penyerang,dan gelandang lalu di masukkan kedalam file txt dengan formasi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan ditaruh di /home/[users]/

**Penyelesaian soal di atas adalah sebagai berikut** 
```
#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>

#define URL "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define FLNAME "players.zip"
#define BUFFER_SIZE 1024

void move_file(char* filename, char* source_dir, char* dest_dir);



int main(void) {

// mendownload file
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char url[256] = {0};
    int a = 0;

    // initialize curl
    curl = curl_easy_init();
    if (!curl) {
        printf("Failed to initialize curl\n");
        return 1;
    }

    snprintf(url, sizeof(url), "%s", URL);
    curl_easy_setopt(curl, CURLOPT_URL, url);


    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);


    fp = fopen(FLNAME, "wb");
    if (!fp) {
        printf("Failed to open file %s\n", FLNAME);
        return 1;
    }

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);


    res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        printf("Failed to download file %s (curl error: %s)\n", FLNAME, curl_easy_strerror(res));
        return 1;
    }


    curl_easy_cleanup(curl);
    fclose(fp);

    printf("File downloaded successfully\n");

//melakukan unzip

    pid_t pid = fork();

    if (pid == 0) {
        // child process
        char *args[] = {"unzip", "players.zip", NULL};
        execvp("unzip", args);
        exit(0);
    } else if (pid > 0) {
        // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Unzip successful.\n");
        } else {
            printf("Unzip failed.\n");
        }
    } else {
        // fork failed
        printf("Fork failed.\n");
        exit(1);
    }

//menghapus file players.zip
  // membuka file players.zip
    int fd = open("players.zip", O_RDONLY);
    if (fd == -1) {
        perror("Error while opening the file");
        exit(EXIT_FAILURE);
    }

    // menghapus file players.zip
    if (unlink("players.zip") == -1) {
        perror("Error while deleting the file");
        exit(EXIT_FAILURE);
    }

    // menutup file
    close(fd);


//memfilter file hanya man.united
char* dir_path = "players";
    DIR* direc = opendir(dir_path);
    if (direc == NULL) {
        perror("Error opening directory");
        return 1;
    }

    struct dirent* entry;
    struct stat file_stat;

    while ((entry = readdir(direc)) != NULL) {
        char file_path[100];
        sprintf(file_path, "%s/%s", dir_path, entry->d_name);

        if (stat(file_path, &file_stat) == -1) {
            perror("Error getting file status");
            continue;
        }

        if (S_ISREG(file_stat.st_mode) && strstr(entry->d_name, "ManUtd") == NULL) {
            if (unlink(file_path) == -1) {
                perror("Error deleting file");
            } else {
                printf("File %s deleted\n", entry->d_name);
            }
        }
    }

    closedir(direc);

//membuat folder bek, gelandang, kiper, penyerang

char *dir_names[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};
   int i, parent_dir_fd;
    for (i = 0; i < 4; i++) {
        if ((parent_dir_fd = open(".", O_RDONLY)) == -1) {
            perror("Error opening parent directory");
            return 1;
        }
        if (mkdirat(parent_dir_fd, dir_names[i], 0700) == -1) {
            perror("Error creating directory");
            return 1;
        }
        if (close(parent_dir_fd) == -1) {
            perror("Error closing parent directory");
            return 1;
        }
    }
    printf("Directories created successfully.\n");


//memfilter file sesuai direktori seperti di soal

	char* dir_name = "players"; // nama direktori yang berisi file player
    char* kiper_dir_name = "Kiper"; // nama direktori untuk file gambar kiper
    char* bek_dir_name = "Bek"; // nama direktori untuk file gambar bek
    char* gelandang_dir_name = "Gelandang"; // nama direktori untuk file gambar gelandang
	char* penyerang_dir_name = "Penyerang";

    DIR* dir = opendir(dir_name); // membuka direktori

    if (dir) // handling jika direktori berhasil dibuka
    {
        struct dirent* entry;
        while ((entry = readdir(dir)) != NULL) // melakukan iterasi seluruh file yang ada pada direktori
        {
            if (entry->d_type == DT_REG) // handling jika file yang ditemukan adalah regular file (bukan direktori)
            {
                char* filename = entry->d_name; // nama file

                if (strstr(filename, "Kiper")) // jika nama file mengandung kata 'kiper'
                {
                    move_file(filename, dir_name, kiper_dir_name); // pindahkan file ke direktori Kiper
                }
                else if (strstr(filename, "Bek")) // jika nama file mengandung kata 'bek'
                {
                    move_file(filename, dir_name, bek_dir_name); // pindahkan file ke direktori Bek
                }
                else if (strstr(filename, "Gelandang")) // jika nama file mengandung kata 'Gelandang'
                {
                    move_file(filename, dir_name, gelandang_dir_name); // pindahkan file ke direktori Gelandang
                }
		else if (strstr(filename, "Penyerang")) // jika nama file mengandung kata penyerang
                {
                    move_file(filename, dir_name, penyerang_dir_name); // pindahkan ke penyerang
                }

            }
        }

        closedir(dir); // menutup direktori
    }
    else // handling jika direktori gagal dibuka
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }


return 0;
}


void move_file(char* namafile, char* scdirektori, char* d_dir)
{
    char source_path[BUFFER_SIZE];
    char dest_path[BUFFER_SIZE];

    // membuat path lengkap untuk file sumber dan tujuan
    snprintf(source_path, BUFFER_SIZE, "%s/%s", scdirektori, namafile);
    snprintf(dest_path, BUFFER_SIZE, "%s/%s", scdirektori, namafile);

    // memindahkan file dari sumber ke tujuan
    int status = rename(source_path, dest_path);

    if (status == 0) // handling jika file berhasil dipindahkan
    {
        printf("File '%s' moved to directory '%s'\n", namafile, d_dir);
    }
    else // handling jika file gagal dipindahkan
    {
        perror("Error moving file");
    }
}
```
**Penjelasan :**
 Pada kode no 3 kami menggunakan beberapa library yang nanti nya akan digunakan untuk menjalankan beberapa fungsi yang ada library library tersebut adalah 
`#include <stdio.h>`,
`#include <curl/curl.h>`,
`#include <stdlib.h>`,
`#include <unistd.h>`,
`#include <sys/wait.h>`,
`#include <sys/stat.h>`,
`#include <sys/types.h>`,
`#include <string.h>`,
`#include <dirent.h>`,
`#include <time.h>`,
`#include <fcntl.h>`.

Setelah itu kami mendefine beberapa konstanta yaitu `URL,FLNAME`,dan `BUFFER_SIZE` Selanjutnya kita masuk ke fungsi mainnya, dalam fungsi main pertama tama kami menginisialisasi beberapa variabel seperti `CURL *curl`,`FILE *fp`,`CURLcode res`,`char url`,dan `int a`. Setelah selesai menginisialisasi beberapa variabel kami langsung membuat langkah langkah untuk mendownload file yang ada di google drive menggunakan beberapa fungsi,yaitu:
- `curl = curl_easy_init()`; Inisialisasi library cURL dan mengalokasikan memori yang dibutuhkan untuk operasi curl yang akan dilakukan.
- `snprintf(url, sizeof(url), "%s", URL)`; Membuat string URL yang akan digunakan untuk download dengan menggunakan fungsi snprintf.
- `curl_easy_setopt(curl, CURLOPT_URL, url)`; Mengatur opsi cURL dengan URL yang telah dibuat sebelumnya.
- `curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L)`; Mengatur opsi cURL agar dapat mengikuti redirect jika URL yang dituju mengalami redirect.
- `fp = fopen(FLNAME, "wb")`; Membuka file dengan mode binary write dan menetapkan nama file yang akan diunduh.
- `curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite)`; Mengatur fungsi yang akan digunakan untuk menulis data yang diunduh ke dalam file.
- `curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp)`; Mengatur file yang akan digunakan untuk menulis data yang diunduh.
`res = curl_easy_perform(curl)`; Melakukan operasi curl untuk mengunduh file dengan URL yang telah ditentukan ke dalam file yang telah dibuka sebelumnya. 
Setelah kami mendownload file tersebut kami cek terlebihdahulu apakah file tersebut berhasil di unduh atau tidak jika tidak maka akang menampilkan pesan eror dan mengembalikan nilai 1.
Setelah selesai mendownload file yang telah ada langsung kami unzip dengan melakukan fork(). 
Setelah selesai mengunzip file kami langsung menghapus file zip yang di unzip. 
Setelah itu file yang telah kami unzip tadi kami filter dengan cara program membuka direktori dengan menggunakan fungsi `opendir()`, kemudian mengecek apakah direktori tersebut berhasil dibuka atau tidak. Jika berhasil dibuka, program akan membaca isi direktori dengan menggunakan fungsi `readdir()`. Kemudian, program akan memeriksa apakah setiap entry dalam direktori merupakan file regular dan tidak mengandung substring "ManUtd" menggunakan fungsi `stat()` dan `strstr()`. Jika memenuhi kriteria, program akan menghapus file tersebut dengan menggunakan fungsi `unlink()`. Jika terjadi kesalahan saat menghapus file, program akan menampilkan pesan error.
lalu, program menutup direktori yang telah dibuka dengan menggunakan fungsi `closedir()`.

Selanjutnya kami membuat folder dengan nama bek,glandang,kiper,dan penyerang

Setelah membuat folder file yang sudah di filter tadi di masukkan kedalam folder folder yang ada sesuai dengan peran masing masing pemain menggunakan fungsi move_file

Fungsi `move_file` akan membuat path lengkap untuk file sumber dan tujuan menggunakan fungsi snprintf. Selanjutnya, fungsi `rename` akan digunakan untuk memindahkan file dari direktori sumber ke direktori tujuan. Jika file berhasil dipindahkan, maka fungsi akan mencetak pesan "File 'namafile' moved to directory 'd_dir'". Namun, jika file gagal dipindahkan, maka fungsi akan mencetak pesan error menggunakan fungsi `perror`.

![](image/No3_filter.jpg)

# No.4
**Pada soal ini, kita diminta untuk menyelesaikan beberapa hal, yaitu:**
1. membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C
2. membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
3. programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai.
4. program ini berjalan dalam background dan hanya menerima satu config cron.

**Penyelesaian dari masalah diatas yaitu:**
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>

void error(char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        error("Invalid number of arguments. Usage: program * HH MM SS /path/to/script.sh");
    }

    char *hour_arg = argv[1];
    char *minute_arg = argv[2];
    char *second_arg = argv[3];
    char *script_path = argv[4];

    int hour, minute, second;
    if (strcmp(hour_arg, "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(hour_arg);
        if (hour < 0 || hour > 23) {
            error("Invalid hour argument. Must be between 0 and 23 or '*'.");
        }
    }

    if (strcmp(minute_arg, "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(minute_arg);
        if (minute < 0 || minute > 59) {
            error("Invalid minute argument. Must be between 0 and 59 or '*'.");
        }
    }

    if (strcmp(second_arg, "*") == 0) {
        second = -1;
    } else {
        second = atoi(second_arg);
        if (second < 0 || second > 59) {
            error("Invalid second argument. Must be between 0 and 59 or '*'.");
        }
    }

    pid_t pid = fork();
    if (pid == -1) {
        error("Failed to fork process.");
    } else if (pid == 0) {
        if (setsid() == -1) {
            error("Failed to create new session.");
        }
        signal(SIGHUP, SIG_IGN);
        pid = fork();
        if (pid == -1) {
            error("Failed to fork process.");
        } else if (pid != 0) {
            exit(EXIT_SUCCESS);
        }
        umask(0);
        if (chdir("/") == -1) {
            error("Failed to change working directory.");
        }
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        int seconds_to_wait;
        while (1) {
            time_t current_time = time(NULL);
            struct tm *local_time = localtime(&current_time);
            if ((hour == -1 || local_time->tm_hour == hour) &&
                (minute == -1 || local_time->tm_min == minute) &&
                (second == -1 || local_time->tm_sec == second)) {
                pid_t script_pid = fork();
                if (script_pid == -1) {
                    error("Failed to fork process.");
                } else if (script_pid == 0) {
                    if (execl(script_path, script_path, NULL) == -1) {
                        error(strerror(errno));
			sprintf("successfull");
                    }
                }
            }
            seconds_to_wait = 60 - local_time->tm_sec;
            sleep(seconds_to_wait);
        }
    } else {
        wait(NULL);
    }

    return 0;
}

```

**Penjelasan dari source code diatas yaitu:**

Pertama-tama, program memeriksa apakah jumlah argumen yang diberikan sesuai dengan yang diharapkan dengan menggunakan `argc`. argumen diinputkan dengan menggunakan `argv`. Argumen yang diinputkan terdiri dari:
1. hour
2. minute
3. second
4. script_path

Jika jumlah argumen kurang dari atau lebih dari 5, program akan memanggil fungsi `error` dan menampilkan pesan kesalahan _"Invalid number of arguments. Usage: program * HH MM SS /path/to/script.sh"_.

Selanjutnya, program memeriksa nilai dari tiga argumen yaitu HH (hour), MM (minute), dan SS (second). Jika argumen ini berisi tanda bintang (*), maka program menganggap bahwa argumen ini tidak penting dan memberikan nilai -1 untuk argumen tersebut. Jika argumen berisi nilai numerik, program akan mengkonversi nilai numerik tersebut menjadi integer dengan beberapa ketentuan tiap argumen, yaitu:
1. 0 < hour < 23 
2. 0 < minute < 59
3. 0 < second < 59

lalu jika sudah sesuai ketentuan diatas akan disimpan dalam variabel `hour`, `minute`, atau `second`.

Program kemudian melakukan fork dan membuat child process. Child process pertama kemudian melakukan `setid()` untuk membuat sesi baru dan kemudian membuat child process kedua. Child process kedua kemudian berjalan di background dan parent process keluar.

Child process kedua kemudian memanggil `umask(0)` untuk mengatur hak akses file dan `chdir("/")` untuk mengubah direktori kerja ke root directory. Kemudian, child process kedua menutup file descriptor stdin, stdout, dan stderr menggunakan `close(STDIN_FILENO)`, `close(STDOUT_FILENO)`, dan `close(STDERR_FILENO)`.

Child process kedua kemudian memasuki loop `while(1)`. Pada setiap iterasi loop, program mendapatkan waktu saat ini dengan memanggil `time(NULL)` dan konversi waktu ke dalam format waktu lokal dengan `localtime(&current_time)`. Program kemudian memeriksa apakah waktu saat ini sama dengan waktu yang ditentukan pada argumen HH, MM, dan SS. Jika waktu saat ini sama dengan waktu yang ditentukan, program akan membuat child process baru dan menjalankan skrip bash yang telah ditentukan dengan menggunakan `execl(script_path, script_path, NULL)`.

Terakhir, parent process menunggu child process keluar dengan menggunakan `wait(NULL)`.

**Hasil programnya:**
1. salah dalam memasukkan argumen Jam
![](image/error_jam.png)
2. salah dalam memasukkan argumen menit
![](image/error_menit.png)
3. salah dalam memasukkan argumen detik
![](image/error_detik.png)
4. argumen yang dimasukkan kurang dari 5 argumen
![](image/error_argumen.png)
5. argumen yang dimasukkan sesuai, dan lanjut ke proses deamonnya
![](image/hasil_daemon.png)

**Kendala pada saat mengerjakan** soal no.4 adalah saat menjalankan proses tersebut menggunakan deamon
