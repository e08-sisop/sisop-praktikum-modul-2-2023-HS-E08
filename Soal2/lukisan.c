#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <dirent.h>
#include <signal.h>

#define FOLDER_NAME_SIZE 20
#define IMAGE_NAME_SIZE 50

volatile sig_atomic_t stop = false;

void sigint_handler(int signum) {
    stop = true;
}

void zip_folder(char *folder_name) {
    char zip_command[100];
    sprintf(zip_command, "zip -qr %s.zip %s", folder_name, folder_name);
    system(zip_command);
}

void delete_folder(char* folder_name) {
    struct dirent *entry;
    DIR *dir = opendir(folder_name);
    char path[PATH_MAX];

    if (dir == NULL) {
        perror("Error while opening the directory");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(path, PATH_MAX, "%s/%s", folder_name, entry->d_name);

            if (entry->d_type == DT_DIR) {
                delete_folder(path);
            } else {
                unlink(path);
            }
        }
    }

    closedir(dir);

    // delete the empty directory
    rmdir(folder_name);
}


int main() {
    int i, j;
    char folder_name[FOLDER_NAME_SIZE], image_name[IMAGE_NAME_SIZE];
    struct tm *tm;
    time_t t;

    // setup signal handler for SIGINT
    signal(SIGINT, sigint_handler);

    while (!stop) {
        // generate folder name
        time(&t);
        tm = localtime(&t);
        strftime(folder_name, FOLDER_NAME_SIZE, "%Y-%m-%d_%H:%M:%S", tm);

        // create folder
        mkdir(folder_name, 0777);

        // download images
        for (i = 0; i < 15; i++) {
            // generate image name
            time(&t);
            tm = localtime(&t);
            sprintf(image_name, "%s/%s_%02d.jpg", folder_name, folder_name, i+1);

            // generate image URL
            char url[30];
            int size = (int)t%1000 + 50;
            sprintf(url, "https://picsum.photos/%d", size);

            // download image
            pid_t pid = fork();
            if (pid == 0) {
                execlp("wget", "wget", "-q", "-O", image_name, url, NULL);
                exit(0);
            }

            // wait for 5 seconds before downloading next image
            sleep(5);
        }

        // wait for child processes to finish
        while (wait(NULL) != -1);

        // zip folder
        zip_folder(folder_name);

        // delete folder
        delete_folder(folder_name);

        // wait for 30 seconds before creating next folder
        sleep(30);
    }

    return 0;
}
