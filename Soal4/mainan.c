#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>

void error(char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        error("Invalid number of arguments. Usage: program * HH MM SS /path/to/script.sh");
    }

    char *hour_arg = argv[1];
    char *minute_arg = argv[2];
    char *second_arg = argv[3];
    char *script_path = argv[4];

    int hour, minute, second;
    if (strcmp(hour_arg, "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(hour_arg);
        if (hour < 0 || hour > 23) {
            error("Invalid hour argument. Must be between 0 and 23 or '*'.");
        }
    }

    if (strcmp(minute_arg, "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(minute_arg);
        if (minute < 0 || minute > 59) {
            error("Invalid minute argument. Must be between 0 and 59 or '*'.");
        }
    }

    if (strcmp(second_arg, "*") == 0) {
        second = -1;
    } else {
        second = atoi(second_arg);
        if (second < 0 || second > 59) {
            error("Invalid second argument. Must be between 0 and 59 or '*'.");
        }
    }

    pid_t pid = fork();
    if (pid == -1) {
        error("Failed to fork process.");
    } else if (pid == 0) {
        if (setsid() == -1) {
            error("Failed to create new session.");
        }
        signal(SIGHUP, SIG_IGN);
        pid = fork();
        if (pid == -1) {
            error("Failed to fork process.");
        } else if (pid != 0) {
            exit(EXIT_SUCCESS);
        }
        umask(0);
        if (chdir("/") == -1) {
            error("Failed to change working directory.");
        }
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        int seconds_to_wait;
        while (1) {
            time_t current_time = time(NULL);
            struct tm *local_time = localtime(&current_time);
            if ((hour == -1 || local_time->tm_hour == hour) &&
                (minute == -1 || local_time->tm_min == minute) &&
                (second == -1 || local_time->tm_sec == second)) {
                pid_t script_pid = fork();
                if (script_pid == -1) {
                    error("Failed to fork process.");
                } else if (script_pid == 0) {
                    if (execl(script_path, script_path, NULL) == -1) {
                        error(strerror(errno));
			sprintf("successfull");
                    }
                }
            }
            seconds_to_wait = 60 - local_time->tm_sec;
            sleep(seconds_to_wait);
        }
    } else {
        wait(NULL);
    }

    return 0;
}

